package com.navori.triggerapp.app;


import android.app.Application;
import android.content.Context;

import com.navori.triggerapp.utility.LocaleHelper;

import java.util.Timer;

public class AppController extends Application {

    private static Context context;
    private Timer timer;

    public void onCreate() {
        super.onCreate();
        AppController.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return AppController.context;
    }


}
