package com.navori.triggerapp.view;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class RegularTextView extends TextView {


    public RegularTextView(Context context) {
        super(context);
        init();
    }



    public RegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RegularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/OpenSans-Regular_1.ttf");
            setTypeface(tf);

    }
   
}
