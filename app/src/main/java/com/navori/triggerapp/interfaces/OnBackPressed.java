package com.navori.triggerapp.interfaces;

public interface OnBackPressed {

     void onBackPressed();
}