package com.navori.triggerapp.model;

import java.util.List;



public class TriggerListModel {

    private List<ItemModel> itemModels;

    public List<ItemModel> getItemModels() {
        return itemModels;
    }

    public void setItemModels(List<ItemModel> itemModels) {
        this.itemModels = itemModels;
    }
}
