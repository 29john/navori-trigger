package com.navori.triggerapp.model;

import android.os.Parcel;
import android.os.Parcelable;



public class PlayListModel implements Parcelable {


    private String name;
    private String duration;
    private String id;
    private String isPlaying;

 public PlayListModel(){}

    private PlayListModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        duration = in.readString();
        isPlaying = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsPlaying() {
        return isPlaying;
    }

    public void setIsPlaying(String isPlaying) {
        this.isPlaying = isPlaying;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(duration);
        dest.writeString(isPlaying);

    }

    public static final Creator<PlayListModel> CREATOR = new Creator<PlayListModel>() {
        public PlayListModel createFromParcel(Parcel in) {
            return new PlayListModel(in);
        }

        public PlayListModel[] newArray(int size) {
            return new PlayListModel[size];

        }
    };


}
