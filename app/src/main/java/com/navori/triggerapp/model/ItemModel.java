package com.navori.triggerapp.model;

import android.os.Parcel;
import android.os.Parcelable;



public class ItemModel implements Parcelable {

    private String id;
    private String name;
    private String type;
    private String trigger;
    private String duration;
    private String isPlaying;


    public  ItemModel(){}

    private ItemModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        type = in.readString();
        trigger = in.readString();
        duration = in.readString();
        isPlaying = in.readString();
    }


    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getIsPlaying() {
        return isPlaying;
    }

    public void setIsPlaying(String isPlaying) {
        this.isPlaying = isPlaying;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(trigger);
        dest.writeString(duration);
        dest.writeString(isPlaying);
    }

    public static final Creator<ItemModel> CREATOR = new Creator<ItemModel>() {
        public ItemModel createFromParcel(Parcel in) {
            return new ItemModel(in);
        }

        public ItemModel[] newArray(int size) {
            return new ItemModel[size];

        }
    };


}
