package com.navori.triggerapp.model;

import java.util.List;



public class PlayListsModel {
    private List<PlayListModel>playListModels;

    public List<PlayListModel> getPlayListModels() {
        return playListModels;
    }

    public void setPlayListModels(List<PlayListModel> playListModels) {
        this.playListModels = playListModels;
    }
}
