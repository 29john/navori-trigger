package com.navori.triggerapp.constant;



public class Constant {

    public static final String SELECTED_LANGUAGE = "selected_language";
    public static final String SELECTED_LANGUAGE_CODE = "selected_language_code";
    public static final String REMOTE_MAC_ADDRESS = "remote_mac_address";
    public static final String REMOTE_DEVICE_NAME = "remote_device_name";
    public static final String MSG_REMOTE_DEVICE_DISCONNECTED = "msg_remote_device_disconnected";
    public static final String LOGIN = "login";
    public static final String LOOP_COUNT = "loop_count";
    public static final String STEP = "step";
    public static final String POSITION = "pos";
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_ERROR = 6;
    public static final int MESSAGE_LOST = 7;
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

}
