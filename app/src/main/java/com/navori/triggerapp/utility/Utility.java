package com.navori.triggerapp.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import java.util.Locale;


public class Utility {


    synchronized public static void savePref(Context context, String field,
                                             String value) {
        SharedPreferences sp = context.getSharedPreferences("com.navori.triggerapp", 0);
        sp.edit().putString(field, value).commit();
    }

    synchronized public static String getPref(Context context, String field,
                                              String def) {
        SharedPreferences sp = context.getSharedPreferences("com.navori.triggerapp", 0);
        return sp.getString(field, def);
    }

    synchronized public static void removePref(Context context, String field) {
        SharedPreferences sp = context.getSharedPreferences("com.navori.triggerapp", 0);
        sp.edit().remove(field);
    }

//    public static void makeToast(String message, Context context) {
//        //Creating the LayoutInflater instance
//        LayoutInflater li = context.getLayoutInflater();
//        //Getting the View object as defined in the customtoast.xml file
//        View layout = li.inflate(R.layout.custom_toast,
//                (ViewGroup)context. findViewById(R.id.custom_toast_layout));
//        Toast toast = null;
//        if (toast == null) {
//            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
//        }
//        if (!toast.getView().isShown()) {
//            toast.setText(message);
//             toast.show();
//        }
//    }

    public static String convertTime(String milliSeconds){

        String result = "0s";
        if(milliSeconds != null && !milliSeconds.equalsIgnoreCase("") && Integer.valueOf(milliSeconds)>=0) {
            long seconds = Long.parseLong(milliSeconds);
            seconds = seconds/1000;
            long s = seconds % 60;
            long m = (seconds / 60) % 60;
            long h = (seconds / (60 * 60)) % 24;
            String str = String.format("%d:%d:%d", h, m, s);
            String[] arr = str.split(":");
            if(Integer.valueOf(arr[2])>=0){
                result = arr[2]+"s";
            }
            if(Integer.valueOf(arr[1])>0){
                result =  arr[1]+"mn" +result;
            }

            if(Integer.valueOf(arr[0])>0){
                result =  arr[0]+"h" +result;
            }

            return result;
        }else{

            return   result;
        }
    }


    public static String getLanguageCode(String strSelectedLanguage) {
        String result = null;
        switch (strSelectedLanguage){
           case "Estonian":
                result ="et";
                break;
            case "French":
                result ="fr";
                break;
            case "English":
                result ="en";
                break;
            case "Chinese":
                result ="zh";
                break;
            case "German":
                result ="de";
                break;
            case "Spanish":
                result ="es";
                break;
            case "Portuguese":
                result ="pt";
                break;
            case "Arabic":
                result ="ar";
                break;
            case "Hindi":
                result ="hi";
                break;
            case "Italian":
                result ="it";
                break;
            case "Japanese":
                result ="ja";
                break;
            case "Dutch":
                result ="nl";
                break;
            case "Malay":
                result ="ms";
                break;
            case "Russian":
                result ="ru";
                break;
            case "Polish":
                result ="pl";
                break;
            case "Norwegian":
                result ="nn";
                break;
            case "Persian":
                result ="pa";
                break;
            case "Thai":
                result ="th";
                break;
            case "Turkish":
                result ="tr";
                break;
            case "Indonesian":
                result ="id";
                break;
            case "Romanian":
                result ="ro";
                break;
            case "Serbian":
                result ="sr";
                break;
            case "Slovak":
                result ="sk";
                break;
            case "Slovenian":
                result ="sl";
                break;
            case "Swedish":
                result ="sv";
                break;
            case "Ukrainian":
                result ="uk";
                break;
            case "Vietnamese":
                result ="vi";
                break;
            case "Lithuanian":
                result ="lt";
                break;
            case "Korean":
                result ="ko";
                break;
            case "Hungarian":
                result ="hu";
                break;
            case "Finnish":
                result ="fi";
                break;
            case "Danish":
                result ="da";
                break;
            case "Croatian":
                result ="hr";
                break;
            case "Bulgarian":
                result ="bg";
                break;
        }
        return result;
    }

    public static  void setupLanguage(Context mContext,String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        mContext.getResources().updateConfiguration(config,
                mContext.getResources().getDisplayMetrics());

    }


}
