package com.navori.triggerapp.service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.navori.triggerapp.constant.Constant;
import com.navori.triggerapp.utility.Utility;

public class ManagePreferenceService extends Service{

    public static boolean  isOnBackPressed = false;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
         return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        isOnBackPressed = true;
        Utility.savePref(this,Constant.REMOTE_MAC_ADDRESS,null);
        Utility.savePref(this,Constant.REMOTE_DEVICE_NAME,null);
        stopSelf();

    }

}
