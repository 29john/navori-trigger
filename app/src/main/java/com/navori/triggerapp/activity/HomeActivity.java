package com.navori.triggerapp.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.navori.triggerapp.R;
import com.navori.triggerapp.app.AppController;
import com.navori.triggerapp.constant.Constant;
import com.navori.triggerapp.fragment.DeviceListFragment;
import com.navori.triggerapp.fragment.HomeFragment;
import com.navori.triggerapp.fragment.SettingFragment;
import com.navori.triggerapp.interfaces.OnBackPressed;
import com.navori.triggerapp.interfaces.OnTimeComplete;
import com.navori.triggerapp.model.DeviceListModel;
import com.navori.triggerapp.model.ItemListModel;
import com.navori.triggerapp.model.ItemModel;
import com.navori.triggerapp.model.PlayListModel;
import com.navori.triggerapp.model.PlayListsModel;
import com.navori.triggerapp.model.TriggerListModel;
import com.navori.triggerapp.service.BluetoothChatService;
import com.navori.triggerapp.service.ManagePreferenceService;
import com.navori.triggerapp.utility.Utility;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    public DrawerLayout drawer;
    public Toolbar toolbar;
    public Activity activity;

    private LinearLayout llHome, llPreferences, llConnectDevice;
    // Local Bluetooth adapter
    public BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    public BluetoothChatService mChatService = null;
    // Intent request codes
    private static final int REQUEST_FORGET_DEVICE = 0;
    private static final int REQUEST_ENABLE_BT = 2;

    public ArrayList<ItemModel> itemModels = new ArrayList<>();
    public ArrayList<ItemModel> triggerModels = new ArrayList<>();
    public ArrayList<ItemModel> nonTriggerModels = new ArrayList<>();
    public ArrayList<PlayListModel> playListModels = new ArrayList<>();
    private Handler handler;
    private TextView tvConnectDevice;
    // Name of the connected device
    private String mConnectedDeviceName = null;
    private ActionBarDrawerToggle toggle;
    public List<DeviceListModel> deviceListModelList = new ArrayList<>();
    public String first = "", middle = "", last = "";
    public String longResponse = "";
    Toast toast = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        activity = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        tvConnectDevice = (TextView) findViewById(R.id.tv_connect_device);
        setSupportActionBar(toolbar);

        // toolbar.setNavigationIcon(R.drawable.playlist);
        showMenuButton();
        DrawerClick();
        setTextForConnect("");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    }

    public void showMenuButton() {
        if (drawer == null) {
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        }
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                // Do whatever you want here
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // Do whatever you want here
            }
        };

        drawer.setDrawerListener(toggle);
        toggle.syncState();

    }


    public void setTextForConnect(String str) {
        if (!TextUtils.isEmpty(str)) {
            tvConnectDevice.setText(str);
            tvConnectDevice.setTextColor(getResources().getColor(R.color.white));
        } else {
            tvConnectDevice.setText(R.string.connect_a_Player);
            tvConnectDevice.setTextColor(getResources().getColor(R.color.red));
        }

    }


    private void DrawerClick() {

        View includedView = findViewById(R.id.included_view);
        llHome = (LinearLayout) includedView.findViewById(R.id.ll_home);
        llPreferences = (LinearLayout) includedView.findViewById(R.id.ll_preferences);
        llConnectDevice = (LinearLayout) includedView.findViewById(R.id.ll_connect);
        llHome.setOnClickListener(this);
        llPreferences.setOnClickListener(this);
        llConnectDevice.setOnClickListener(this);
        tvConnectDevice.setOnClickListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(), "HomeFragment").commit();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_preferences:
                drawer.closeDrawer(GravityCompat.START);
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new SettingFragment(), "SettingFragment").commit();
                break;

            case R.id.ll_home:
                drawer.closeDrawer(GravityCompat.START);
                getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(), "HomeFragment").commit();

                break;

            case R.id.ll_connect:
                drawer.closeDrawer(GravityCompat.START);

                if (mChatService.getState() == BluetoothChatService.STATE_CONNECTED) {
                    showAlert();
                } else {
                    switchToDeviceList(true);
                }

                break;
            case R.id.tv_connect_device:
                if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
                    switchToDeviceList(true);
                }
                break;

        }
    }

    public void resetApp() {
        if (mChatService != null) mChatService.stop();
        SharedPreferences sp = getSharedPreferences("com.navori.triggerapp", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear().commit();

        Context context = AppController.getAppContext();
        Intent mStartActivity = new Intent(context, HomeActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, mPendingIntent);

        if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
            ((ActivityManager) context.getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData();
        } else {
            clearApplicationData();
        }
        System.exit(0);


    }

    public void clearApplicationData() {
        File cacheDirectory = getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    public static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }

        return deletedAll;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            if (mChatService == null) setupChat();
            if (mChatService.getState() == BluetoothChatService.STATE_CONNECTED && itemModels.isEmpty()) {
                String str = "hi";
                try {
                    mChatService.write(str.getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();

        if (mChatService != null) {
            if (mChatService.getState() == BluetoothChatService.STATE_CONNECTED && itemModels.isEmpty()) {
                String str = "hi";
                try {
                    mChatService.write(str.getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return;
            }
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                mChatService.start();
            }
            if (mChatService.getState() == BluetoothChatService.STATE_STARTED) {
                if (Utility.getPref(this, Constant.REMOTE_MAC_ADDRESS, null) != null) {
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(Utility.getPref(this, Constant.REMOTE_MAC_ADDRESS, null));
                    if (mChatService != null) {
                        mChatService.connect(device);
                    } else {
                        mChatService = BluetoothChatService.GetInstance(this, mHandler);
                        mChatService.connect(device);
                    }
                }
            }
        }

    }

    private void setupChat() {
        // Initialize the BluetoothChatService to perform bluetooth connections
        mChatService = BluetoothChatService.GetInstance(this, mHandler);

    }

    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constant.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    break;
                case Constant.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.e("MESSAGE_READ Response", readMessage);

                    if (readMessage.startsWith("{") && readMessage.endsWith("}")) {
                        parseResponse(readMessage);
                    } else if (readMessage.startsWith("{")) {
                        first = "";
                        first = readMessage;
                    } else if (!readMessage.startsWith("{") && !readMessage.endsWith("}")) {
                        if(readMessage.contains("&@#¶")) {
                            startHandler(readMessage);
                        }else if(readMessage.contains("NoTriggerAddOn")){
                            parseResponse(readMessage);
                        }else {
                            if (first.length() > 0) {
                                middle = middle + readMessage;
                            } else if (last.length() > 0) {
                                middle = readMessage + middle;
                            }else{
                                String str = "hi";
                                try {
                                    mChatService.write(str.getBytes("UTF-8"));
                                } catch (UnsupportedEncodingException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    } else if (readMessage.endsWith("}")) {
                        last = "";
                        last = readMessage;
                        loaddata();
                    }


                    break;
                case Constant.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constant.DEVICE_NAME);
                    String str = "hi";
                    try {
                        mChatService.write(str.getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    setTextForConnect(getResources().getString(R.string.connected_to)+" " + Utility.getPref(activity, Constant.REMOTE_DEVICE_NAME, null));
                    Fragment deviceList = (Fragment) getSupportFragmentManager().findFragmentByTag("DeviceListFragment");
                    if (deviceList != null && deviceList.isVisible()) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(), "HomeFragment").commit();
                    }

                    break;
                case Constant.MESSAGE_TOAST:
                    setTextForConnect(msg.getData().getString(Constant.TOAST));
                    playListModels.clear();
                    nonTriggerModels.clear();
                    triggerModels.clear();
                    if (msg.getData().getString(Constant.TOAST).equalsIgnoreCase(getResources().getString(R.string.connect_a_Player))) {
                        Fragment home = (Fragment) getSupportFragmentManager().findFragmentByTag("HomeFragment");
                        if (home == null && !isFinishing()) {
                            getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(), "HomeFragment").commit();
                        }

                    }
                    broadcast();

                    break;

            }
        }
    };

    private void startHandler(String readMessage) {
    String [] result = readMessage.split("&@#¶");
      if(result.length==2){
          for(int i = 0; i<playListModels.size();i++){
              if(playListModels.get(i).getId().equalsIgnoreCase(result[0])){
                  if (Integer.valueOf(result[1]) > 0) {
                      startTimer(playListModels.get(i).getId(),""+(Integer.valueOf(playListModels.get(i).getDuration())*Integer.valueOf(result[1])),i);
                  }

              }
          }
      }else if(result.length ==3){
           for(int i = 0; i<triggerModels.size();i++){
               if(triggerModels.get(i).getId().equalsIgnoreCase(result[0])){
                   if (Integer.valueOf(result[1]) > 0) {
                       startTimer(triggerModels.get(i).getId(),""+(Integer.valueOf(triggerModels.get(i).getDuration())*Integer.valueOf(result[1])),i);
                     break;
                   }

               }
           }
          for(int i = 0; i<nonTriggerModels.size();i++){
               if(nonTriggerModels.get(i).getId().equalsIgnoreCase(result[0])){
                   if (Integer.valueOf(result[1]) > 0) {
                       startTimer(nonTriggerModels.get(i).getId(),""+(Integer.valueOf(nonTriggerModels.get(i).getDuration())*Integer.valueOf(result[1])),i);
                       break;
                   }

               }
           }

      }
    }

    private void loaddata() {
        if (first.length() > 0 && last.length() > 0) {
            parseResponse(first + middle + last);
        }
    }


    private void parseResponse(String readMessage) {

        Log.e("Parse Response", readMessage);
        if (readMessage.contains("NoTriggerAddOn")) {
            try {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(this);
                }
                builder.setTitle("Navori QL RC")
                        .setMessage(getResources().getString(R.string.appbar_scrolling_view_behavior))
                        .setPositiveButton(R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Stop the Bluetooth chat services
                                        if (mChatService != null) mChatService.stop();
                                        if (mChatService == null) setupChat();
                                        if (mChatService != null) mChatService.start();
                                    }
                                }
                        )
                        .show();

            } catch (Exception e) {
            }
            makeToast(getResources().getString(R.string.appbar_scrolling_view_behavior), this);
            return;
        }

        try {
            JSONObject object = new JSONObject(readMessage);
            List<String> idsList = new ArrayList<>();
            List<String> typeList = new ArrayList<>();
            List<String> durationList = new ArrayList<>();
            List<String> nameList = new ArrayList<>();
            List<String> triggerList = new ArrayList<>();

            List<String> playIdsList = new ArrayList<>();
            List<String> playNameList = new ArrayList<>();
            List<String> playDurationList = new ArrayList<>();

            JSONArray ids = object.getJSONArray("ids");
            if (ids.length() > 0) {
                JSONArray childIds = ids.getJSONArray(0);

                for (int i = 0; i < childIds.length(); i++) {
                    idsList.add("" + childIds.get(i));
                }
            }
            JSONArray types = object.getJSONArray("types");
            if (types.length() > 0) {
                JSONArray childtypes = types.getJSONArray(0);
                for (int i = 0; i < childtypes.length(); i++) {
                    typeList.add("" + childtypes.get(i));
                }
            }
            JSONArray duration = object.getJSONArray("duration");
            if (duration.length() > 0) {
                JSONArray childduration = duration.getJSONArray(0);
                for (int i = 0; i < childduration.length(); i++) {
                    durationList.add("" + childduration.get(i));
                }
            }

            JSONArray names = object.getJSONArray("names");
            for (int i = 0; i < names.length(); i++) {
                nameList.add("" + names.get(i));
            }
            JSONArray trigger1 = object.getJSONArray("trigger");
            if (trigger1.length() > 0) {
                JSONArray childtrigger1 = trigger1.getJSONArray(0);
                for (int i = 0; i < childtrigger1.length(); i++) {
                    triggerList.add("" + childtrigger1.get(i));
                }
            }


            itemModels.clear();
            ItemModel itemModel = new ItemModel();

            for (int m = 0; m < nameList.size(); m++) {
                itemModel = new ItemModel();
                itemModel.setName(nameList.get(m));
                if (typeList != null && typeList.size() > m)
                    itemModel.setType(typeList.get(m));
                if (idsList != null && idsList.size() > m)
                    itemModel.setId(idsList.get(m));
                if (triggerList != null && triggerList.size() > m)
                    itemModel.setTrigger(triggerList.get(m));
                if (durationList != null && durationList.size() > m)
                    itemModel.setDuration(durationList.get(m));
                itemModel.setIsPlaying("false");
                itemModels.add(itemModel);
            }
            JSONArray playIds = object.getJSONArray("playId");
            if (playIds.length() > 0) {
                JSONArray childPlayIds = playIds.getJSONArray(0);
                for (int i = 0; i < childPlayIds.length(); i++) {
                    playIdsList.add("" + childPlayIds.get(i));
                }
            }
            JSONArray playname = object.getJSONArray("playName");
            for (int i = 0; i < playname.length(); i++) {
                playNameList.add("" + playname.get(i));
            }
            JSONArray playDuration = object.getJSONArray("playDuration");
            if (playDuration.length() > 0) {
                JSONArray childPlayDuration = playDuration.getJSONArray(0);
                for (int i = 0; i < childPlayDuration.length(); i++) {
                    playDurationList.add("" + childPlayDuration.get(i));
                }
            }

            playListModels.clear();
            PlayListModel playListModel = new PlayListModel();
            for (int m = 0; m < playIdsList.size(); m++) {
                playListModel = new PlayListModel();
                playListModel.setId(playIdsList.get(m));
                if (playNameList != null && playNameList.size() > m)
                    playListModel.setName(playNameList.get(m));
                if (playDurationList != null && playDurationList.size() > m)
                    playListModel.setDuration(playDurationList.get(m));
                playListModel.setIsPlaying("false");
                playListModels.add(playListModel);
            }
            setRecord();

        } catch (JSONException e) {

            String str = "hi";
            try {
                mChatService.write(str.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            Log.e("Response:", "Invalid response");
            e.printStackTrace();
        }

    }

    private void setRecord() {
        if (itemModels.size() > 0 || playListModels.size() > 0) {
            makeToast(getResources().getString(R.string.media_loaded), this);
        } else {
            makeToast(getResources().getString(R.string.no_media_found), this);
        }

        triggerModels.clear();
        for (int i = 0; i < itemModels.size(); i++) {
            if (itemModels.get(i).getTrigger().equalsIgnoreCase("true")) {
                triggerModels.add(itemModels.get(i));
            }
        }
        nonTriggerModels.clear();
        for (int i = 0; i < itemModels.size(); i++) {
            if (itemModels.get(i).getTrigger().equalsIgnoreCase("false")) {
                nonTriggerModels.add(itemModels.get(i));
            }
        }

        broadcast();
    }

    public void broadcast() {
        broadcastNonTrigger();
        broadcastPlayList();
        broadcastTrigger();

    }

    public void broadcastPlayList() {

        PlayListsModel playListsModel = new PlayListsModel();
        playListsModel.setPlayListModels(playListModels);
        EventBus.getDefault().postSticky(playListsModel);

    }

    public void broadcastTrigger() {


        TriggerListModel triggerListModel = new TriggerListModel();
        triggerListModel.setItemModels(itemModels);
        EventBus.getDefault().postSticky(triggerListModel);

    }

    public void broadcastNonTrigger() {

        ItemListModel itemListModel = new ItemListModel();
        itemListModel.setItemModels(itemModels);
        EventBus.getDefault().postSticky(itemListModel);

    }

    @Override
    public synchronized void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    private void switchToDeviceList(boolean value) {
        Utility.savePref(HomeActivity.this, Constant.REMOTE_MAC_ADDRESS, null);
        setTextForConnect("");
        playListModels.clear();
        nonTriggerModels.clear();
        triggerModels.clear();
        broadcast();
        if (!isFinishing() && value)
            getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new DeviceListFragment(), "DeviceListFragment").commit();


    }

    // hide SoftKeyboard
    public void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void startTimer(final String id, String Time, final int position) {

        // do stuff
        final Fragment trigger = (Fragment) getSupportFragmentManager().findFragmentByTag("TriggerMediaFragment");
        final Fragment nonTrigger = (Fragment) getSupportFragmentManager().findFragmentByTag("NonTriggerMediaFragment");
        final Fragment playlist = (Fragment) getSupportFragmentManager().findFragmentByTag("PlayListFragment");
        if (trigger != null && trigger.isVisible()) {
            for (int i = 0; i < playListModels.size(); i++) {
                if (playListModels.get(i).getIsPlaying().equalsIgnoreCase("true")) {
                    playListModels.get(i).setIsPlaying("false");
                    broadcastPlayList();
                }
            }
            for (int i = 0; i < nonTriggerModels.size(); i++) {
                if (nonTriggerModels.get(i).getIsPlaying().equalsIgnoreCase("true")) {
                    nonTriggerModels.get(i).setIsPlaying("false");
                    broadcastNonTrigger();
                }
            }
        }
        if (nonTrigger != null && nonTrigger.isVisible()) {
            for (int i = 0; i < playListModels.size(); i++) {
                if (playListModels.get(i).getIsPlaying().equalsIgnoreCase("true")) {
                    playListModels.get(i).setIsPlaying("false");
                    broadcastPlayList();
                }
            }
            for (int i = 0; i < triggerModels.size(); i++) {
                if (triggerModels.get(i).getIsPlaying().equalsIgnoreCase("true")) {
                    triggerModels.get(i).setIsPlaying("false");
                    broadcastTrigger();
                }
            }
        }

        if (playlist != null && playlist.isVisible()) {
            for (int i = 0; i < triggerModels.size(); i++) {
                if (triggerModels.get(i).getIsPlaying().equalsIgnoreCase("true")) {
                    triggerModels.get(i).setIsPlaying("false");
                    broadcastTrigger();
                }
            }
            for (int i = 0; i < nonTriggerModels.size(); i++) {
                if (nonTriggerModels.get(i).getIsPlaying().equalsIgnoreCase("true")) {
                    nonTriggerModels.get(i).setIsPlaying("false");
                    broadcastNonTrigger();
                }
            }
        }

        if (handler != null) handler.removeCallbacks(null);
        int time = Integer.valueOf(Time);
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (trigger != null && trigger.isVisible()) {
                    ((OnTimeComplete) trigger).onTimeComplete(id, position);
                } else if (nonTrigger != null && nonTrigger.isVisible()) {
                    ((OnTimeComplete) nonTrigger).onTimeComplete(id, position);
                } else if (playlist != null && playlist.isVisible()) {
                    ((OnTimeComplete) playlist).onTimeComplete(id, position);
                } else if (nonTriggerModels.size() > position && nonTriggerModels.get(position).getId().equalsIgnoreCase(id)) {
                    nonTriggerModels.get(position).setIsPlaying("false");
                } else if (triggerModels.size() > position && triggerModels.get(position).getId().equalsIgnoreCase(id)) {
                    triggerModels.get(position).setIsPlaying("false");

                } else if (playListModels.size() > position && playListModels.get(position).getId().equalsIgnoreCase(id)) {
                    playListModels.get(position).setIsPlaying("false");

                }
            }
        }, time);


    }

    public void cancelTimer() {
        if (handler != null) handler.removeCallbacks(null);
    }


    public void releasePlayerApp() {
        if (Utility.getPref(activity, Constant.REMOTE_DEVICE_NAME, null) != null && mChatService.getState() == BluetoothChatService.STATE_CONNECTED) {
            String str = "release";
            try {
                mChatService.write(str.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {

            makeToast(getResources().getString(R.string.not_connected), this);
            switchToDeviceList(false);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();
        if (handler != null) handler.removeCallbacks(null);
        Utility.savePref(HomeActivity.this, Constant.REMOTE_DEVICE_NAME, null);
        Utility.savePref(HomeActivity.this, Constant.REMOTE_MAC_ADDRESS, null);
    }

    public void refreshRecord() {

        if (Utility.getPref(activity, Constant.REMOTE_DEVICE_NAME, null) != null && mChatService.getState() == BluetoothChatService.STATE_CONNECTED) {
            String str = "hi";
            try {
                mChatService.write(str.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            makeToast(getResources().getString(R.string.not_connected), this);

        }

    }

    @Override
    public void onBackPressed() {
        ManagePreferenceService.isOnBackPressed = true;
        Fragment home = (Fragment) getSupportFragmentManager().findFragmentByTag("HomeFragment");
        Fragment trigger = (Fragment) getSupportFragmentManager().findFragmentByTag("TriggerMediaFragment");
        Fragment nonTrigger = (Fragment) getSupportFragmentManager().findFragmentByTag("NonTriggerMediaFragment");
        Fragment playlist = (Fragment) getSupportFragmentManager().findFragmentByTag("PlayListFragment");
        Fragment selectLanguage = (Fragment) getSupportFragmentManager().findFragmentByTag("SelectionLanguageFragment");
        Fragment deviceList = (Fragment) getSupportFragmentManager().findFragmentByTag("DeviceListFragment");
        Fragment setting = (Fragment) getSupportFragmentManager().findFragmentByTag("SettingFragment");
        if (home != null && home.isVisible()) {
            if (mChatService.getState() == BluetoothChatService.STATE_CONNECTED) {
                showAlert();
            } else {

                showAlertForExit();
            }
        } else if (trigger != null && trigger.isVisible()) {
            ((OnBackPressed) trigger).onBackPressed();
        } else if (nonTrigger != null && nonTrigger.isVisible()) {
            ((OnBackPressed) nonTrigger).onBackPressed();
        } else if (playlist != null && playlist.isVisible()) {
            ((OnBackPressed) playlist).onBackPressed();
        } else if (selectLanguage != null && selectLanguage.isVisible()) {
            ((OnBackPressed) selectLanguage).onBackPressed();
        } else if (deviceList != null && deviceList.isVisible()) {
            ((OnBackPressed) deviceList).onBackPressed();
        } else if (setting != null && setting.isVisible()) {
            ((OnBackPressed) setting).onBackPressed();
        }

    }

    private void showAlert() {

        final Dialog dialog = new Dialog(HomeActivity.this, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.disconnect_alert_msg);
        TextView tvYes = (TextView) dialog.findViewById(R.id.tv_yes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.tv_no);
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                longResponse = "";
                switchToDeviceList(false);
                mChatService.stop();

            }
        });
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }


    private void showAlertForExit() {

        final Dialog dialog = new Dialog(HomeActivity.this, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.exit_the_app);
        TextView tvYes = (TextView) dialog.findViewById(R.id.tv_yes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.tv_no);
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Utility.savePref(HomeActivity.this, Constant.REMOTE_MAC_ADDRESS, null);
                Utility.savePref(HomeActivity.this, Constant.REMOTE_DEVICE_NAME, null);
                finish();

            }
        });
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }


    public void showResetAlert() {

        final Dialog dialog = new Dialog(HomeActivity.this, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.reset_alert_msg);
        TextView tvYes = (TextView) dialog.findViewById(R.id.tv_yes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.tv_no);
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                resetApp();

            }
        });
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    public void makeToast(String message, Context context) {

        if (toast == null) {
            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        }
        if (!toast.getView().isShown()) {
            toast.setText(message);
            toast.show();
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occured
                    makeToast(getResources().getString(R.string.bt_not_enabled_leaving), HomeActivity.this);
                    finish();
                }
                break;
        }
    }

    // Every 1 minute, check and make device visible
    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
            startActivity(discoverableIntent);
        }
    }

    private Timer MakeVisibleTimer = null;

    public class MakeVisibleTimerTask extends TimerTask {
        @Override
        public void run() {
            ensureDiscoverable();
        }
    }

    public void StartTimer() {
        if (MakeVisibleTimer != null) {
            MakeVisibleTimer.cancel();
            MakeVisibleTimer.purge();
            MakeVisibleTimer = null;
        }
        MakeVisibleTimer = new Timer("MakeVisibleTimer");
        MakeVisibleTimerTask MakeVisibleTask = new MakeVisibleTimerTask();
        MakeVisibleTimer.schedule(MakeVisibleTask, 1000, 60000);
    }
}
