package com.navori.triggerapp.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aigestudio.wheelpicker.WheelPicker;
import com.navori.triggerapp.R;
import com.navori.triggerapp.constant.Constant;
import com.navori.triggerapp.service.ManagePreferenceService;
import com.navori.triggerapp.utility.Utility;

import java.util.Arrays;
import java.util.List;


public class LanguageSelectionActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvOk;
    private String strSelectedLanguage = "";
    private WheelPicker wheelPicker;
    private Context mContext;
    private Typeface typeface;
    private int pos = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!TextUtils.isEmpty(Utility.getPref(this, Constant.SELECTED_LANGUAGE, null))) {
            switchToHome();
        }

        setContentView(R.layout.activity_language_selection);
        mContext = this;
        typeface = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular_1.ttf");

        wheelPicker = (WheelPicker) findViewById(R.id.wheel_languages);
        tvOk = (TextView) findViewById(R.id.tv_ok);

        wheelPicker.setTypeface(typeface);

        tvOk.setOnClickListener(this);

        String[] language_array = getResources().getStringArray(R.array.languages);
        List<String> data = Arrays.asList(language_array);
        wheelPicker.setData(data);

        if(TextUtils.isEmpty(Utility.getPref(mContext,Constant.POSITION,null))){
            wheelPicker.setSelectedItemPosition(2);
            strSelectedLanguage ="English";
        }else{
            wheelPicker.setSelectedItemPosition(Integer.valueOf(Utility.getPref(mContext,Constant.POSITION,null)));
        }

        wheelPicker.setCyclic(false);
        wheelPicker.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                strSelectedLanguage = String.valueOf(data);
                pos = position;
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (!strSelectedLanguage.equalsIgnoreCase("")) {
            Utility.savePref(mContext, Constant.SELECTED_LANGUAGE, strSelectedLanguage);
            Utility.savePref(mContext,Constant.POSITION,""+pos);
            Utility.savePref(mContext, Constant.SELECTED_LANGUAGE_CODE, Utility.getLanguageCode(strSelectedLanguage));
            switchToHome();
        } else {
            makeToast("Please select any language", mContext);
        }
    }




    private void switchToHome() {
        startService(new Intent(getBaseContext(), ManagePreferenceService.class));
        Utility.setupLanguage(this, Utility.getPref(this, Constant.SELECTED_LANGUAGE_CODE, null));
        Intent intent = new Intent(LanguageSelectionActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void makeToast(String message, Context context) {
        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        View layout = li.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView textView = (TextView) layout.findViewById(R.id.custom_toast_message);
        Toast toast = null;
        if (toast == null) {
            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        }
        if (!toast.getView().isShown()) {
            textView.setText(message);
            toast.setView(layout);
            toast.show();
        }
    }

}
