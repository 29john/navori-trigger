package com.navori.triggerapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.navori.triggerapp.R;
import com.navori.triggerapp.activity.HomeActivity;
import com.navori.triggerapp.adapter.GridAdapter;
import com.navori.triggerapp.adapter.PlayListAdapter;
import com.navori.triggerapp.model.PlayListModel;
import com.navori.triggerapp.model.PlayListsModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements View.OnClickListener {

    public HomeFragment() {
    }


    private HomeActivity mContext;
    private View root;
    private GridView gridView;
    private ArrayList<String> mList = new ArrayList<>();
    private GridAdapter gridAdapter;
    private ImageView searchIcon;
    private TextView tvHeader;
    private RelativeLayout rlHeader;
    private LinearLayout llDisable;
    private ImageView imgRefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.grid_layout, container, false);
        mContext = (HomeActivity) getActivity();
        rlHeader = (RelativeLayout)mContext.findViewById(R.id.rl_header);
        rlHeader.setVisibility(View.VISIBLE);
        tvHeader =(TextView)mContext.findViewById(R.id.header_text);
        tvHeader.setText(R.string.remote_controller);
        searchIcon = (ImageView)mContext.findViewById(R.id.search_icon);
        searchIcon.setVisibility(View.GONE);

        imgRefresh =(ImageView)mContext.findViewById(R.id.img_refresh);
        imgRefresh.setVisibility(View.GONE);
        imgRefresh.setOnClickListener(this);
        mContext.showMenuButton();
        mContext.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
         gridView = (GridView)root.findViewById(R.id.grid_view);
        llDisable =(LinearLayout)root.findViewById(R.id.ll_disable);
        llDisable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mList.add("1");
        mList.add("2");
        mList.add("3");

        gridAdapter = new GridAdapter(mContext,mList);
        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        if(mContext.nonTriggerModels.size()>0)
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new NonTriggerMediaFragment(), "NonTriggerMediaFragment").commit();
                        break;

                    case 1:
                        if(mContext.playListModels.size()>0)
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new PlayListFragment(), "PlayListFragment").commit();
                        break;

                    case 2:
                        if(mContext.triggerModels.size()>0)
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new TriggerMediaFragment(), "TriggerMediaFragment").commit();
                        break;
                }
            }
        });

        return root;
    }

    // UI updates must run on MainThread
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(PlayListsModel playListsModel) {

        PlayListsModel stickyEvent = EventBus.getDefault().getStickyEvent(PlayListsModel.class);
        if(stickyEvent != null) {
            // "Consume" the sticky event
            if(mContext.playListModels.size()>0 || mContext.nonTriggerModels.size()>0 || mContext.triggerModels.size()>0) {
                llDisable.setVisibility(View.GONE);
                imgRefresh.setVisibility(View.VISIBLE);
                gridAdapter = new GridAdapter(mContext,mList);
                gridView.setAdapter(gridAdapter);
            }else{
                llDisable.setVisibility(View.VISIBLE);
                imgRefresh.setVisibility(View.GONE);
                gridAdapter = new GridAdapter(mContext,mList);
                gridView.setAdapter(gridAdapter);
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_refresh:
                mContext.refreshRecord();
                break;

        }
    }
}
