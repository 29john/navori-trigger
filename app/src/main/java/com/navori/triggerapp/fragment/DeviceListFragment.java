package com.navori.triggerapp.fragment;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.navori.triggerapp.R;
import com.navori.triggerapp.activity.HomeActivity;
import com.navori.triggerapp.adapter.DeviceListAdapter;
import com.navori.triggerapp.constant.Constant;
import com.navori.triggerapp.interfaces.OnBackPressed;
import com.navori.triggerapp.model.DeviceListModel;
import com.navori.triggerapp.utility.Utility;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static android.content.ContentValues.TAG;

public class DeviceListFragment extends Fragment implements OnBackPressed, View.OnClickListener {

    public DeviceListFragment() {
    }



    private BluetoothAdapter mBtAdapter;
  private HomeActivity mContext;
    private View root;
    private TextView tvHeader;
    private ListView pairedListView;
    private TextView tvNoRecord;
    private DeviceListAdapter deviceListAdapter;
    private ImageView searchIcon,imgRefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.device_list_fragment, container, false);
        mContext = (HomeActivity) getActivity();
        tvHeader = (TextView) mContext.findViewById(R.id.header_text);
        tvHeader.setText(R.string.select_a_player);
        searchIcon = (ImageView)mContext.findViewById(R.id.search_icon);
        searchIcon.setVisibility(View.GONE);
        imgRefresh = (ImageView) mContext. findViewById(R.id.img_refresh);
        imgRefresh.setVisibility(View.VISIBLE);
        imgRefresh.setOnClickListener(this);

        mContext.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mContext.showMenuButton();
        tvNoRecord = (TextView) root.findViewById(R.id.no_paired_device);
         pairedListView = (ListView) root.findViewById(R.id.paired_devices);
        deviceListAdapter = new DeviceListAdapter(getActivity(), mContext.deviceListModelList);
        pairedListView.setAdapter(deviceListAdapter);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        pairedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mBtAdapter.cancelDiscovery();

                mContext.playListModels.clear();
                mContext.triggerModels.clear();
                mContext.nonTriggerModels.clear();
                mContext.broadcast();

                ensureDiscoverable();

                // Get the device MAC address, which is the last 17 chars in the View
                String address = mContext.deviceListModelList.get(position).getMacAddress();
                // Get the BLuetoothDevice object
                BluetoothDevice device = mContext.mBluetoothAdapter.getRemoteDevice(address);

                Utility.savePref(mContext, Constant.REMOTE_MAC_ADDRESS, address);
                Utility.savePref(mContext, Constant.REMOTE_DEVICE_NAME, mContext.deviceListModelList.get(position).getDeviceName());

                mContext.mChatService.stop();
                mContext.mChatService.start();
               // Attempt to connect to the device
                mContext.mChatService.connect(device);

            }
        });

        return root;

    }
    private void ensureDiscoverable() {
        if (mBtAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
            mContext.startActivity(discoverableIntent);
        }
        else
        {
            Method method;
            try {
                method = mBtAdapter.getClass().getMethod("setScanMode", int.class, int.class);
                method.invoke(mBtAdapter,BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE,120);
                Log.e("invoke","method invoke successfully");
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        mContext.registerReceiver(mReceiver, filter);
        showPairedDevice();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Make sure we're not doing discovery anymore
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }
        // Unregister broadcast listeners
        mContext.unregisterReceiver(mReceiver);
    }


    @Override
    public void onBackPressed() {

        mContext.getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(),"HomeFragment").commit();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.img_refresh:
              mContext.showResetAlert();
                break;

        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive (Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                showPairedDevice();
//                if(intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)== BluetoothAdapter.STATE_OFF){
//                    showPairedDevice();
//                }else{
//                    showPairedDevice();
//                }

            }

        }

    };

    public void showPairedDevice() {
        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices.size() > 0) {
            mContext.deviceListModelList.clear();
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                try {
                    Method method = device.getClass().getMethod("getAliasName");
                    if(method != null) {
                        deviceName = (String)method.invoke(device);
                    }
                }
                catch (Exception ex) {}
                DeviceListModel  deviceListModel = new DeviceListModel();
                deviceListModel.setDeviceName(deviceName);
                deviceListModel.setMacAddress(device.getAddress());
                mContext.deviceListModelList.add(deviceListModel);

            }
            root.findViewById(R.id.no_paired_device).setVisibility(View.GONE);
            pairedListView.setVisibility(View.VISIBLE);
            deviceListAdapter.notifyDataSetChanged();
        } else {
           tvNoRecord.setVisibility(View.VISIBLE);
            if(!mBtAdapter.isEnabled()){
                tvNoRecord.setText(R.string.bt_not_enabled_leaving);
            }else{
                tvNoRecord.setText(R.string.no_paired_device);
            }

            pairedListView.setVisibility(View.GONE);
        }

    }
}
