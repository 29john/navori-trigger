package com.navori.triggerapp.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.navori.triggerapp.R;
import com.navori.triggerapp.activity.HomeActivity;
import com.navori.triggerapp.adapter.PlayListAdapter;
import com.navori.triggerapp.constant.Constant;
import com.navori.triggerapp.interfaces.OnBackPressed;
import com.navori.triggerapp.interfaces.OnTimeComplete;
import com.navori.triggerapp.model.PlayListModel;
import com.navori.triggerapp.model.PlayListsModel;
import com.navori.triggerapp.utility.Utility;
import com.navori.triggerapp.view.CustomEditText;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class PlayListFragment extends Fragment implements View.OnClickListener,OnBackPressed,OnTimeComplete,PlayListAdapter.MyClickListener {


    public PlayListFragment(){}

    private View view;
    private ListView listView;
    private PlayListAdapter playListAdapter;
    private ArrayList<PlayListModel> playListModelList = new ArrayList<>();
    private HomeActivity mActivity;
    private ImageView searchIcon;
    private TextView tvHeader;
    private CustomEditText customEditText;
    private ImageView imgRefresh;
    private int Count = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.list_fragment, container, false);
        listView =(ListView)view.findViewById(R.id.custom_listview);
        mActivity = (HomeActivity) getActivity();
        searchIcon = (ImageView)mActivity.findViewById(R.id.search_icon);
        searchIcon.setVisibility(View.VISIBLE);
        searchIcon.setOnClickListener(this);
        imgRefresh = (ImageView) mActivity. findViewById(R.id.img_refresh);
        imgRefresh.setOnClickListener(this);

        tvHeader =(TextView)mActivity.findViewById(R.id.header_text);
        tvHeader.setText(R.string.playlist);

        mActivity.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mActivity.toolbar.setNavigationIcon(R.drawable.home_white);
        customEditText = (CustomEditText)mActivity.findViewById(R.id.searchEditText);
        playListModelList.addAll(mActivity.playListModels);
        playListAdapter = new PlayListAdapter(getActivity(),playListModelList,this);
        listView.setAdapter(playListAdapter);
        mActivity.toolbar.setNavigationOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(),"HomeFragment").commit();

            }
        });

        customEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
        customEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));

        customEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    mActivity.hideSoftKeyboard();
                    if(customEditText.getText().length()>0){
                        playListAdapter.getFilter().filter(customEditText.getText());
                    }

                }
                return false;
            }
        });

        customEditText.setInputType(InputType.TYPE_CLASS_TEXT);
        customEditText.requestFocus();
        customEditText.setDrawableClickListener(new CustomEditText.DrawableClickListener() {
            public void onClick(DrawablePosition target) {
                switch (target) {
                    case RIGHT:
                        customEditText.getText().clear();
                        playListAdapter.getFilter().filter("");
                         break;

                    case LEFT:
                        mActivity.hideSoftKeyboard();
                        customEditText.getText().clear();
                        customEditText.setVisibility(View.GONE);
                        mActivity.toolbar.setVisibility(View.VISIBLE);
                        playListAdapter.getFilter().filter("");

                    default:
                        break;
                }
            }

        });

        customEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    customEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_back, 0, R.drawable.ic_close, 0);
                    playListAdapter.getFilter().filter(s);

                } else {
                    customEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_back, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        return view;
    }

    // UI updates must run on MainThread
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(PlayListsModel playListsModel) {


        PlayListsModel stickyEvent = EventBus.getDefault().getStickyEvent(PlayListsModel.class);
// Better check that an event was actually posted before
        if(stickyEvent != null) {
            // "Consume" the sticky event
            if(mActivity.playListModels.size()>0) {
                playListModelList.clear();
                playListModelList.addAll(mActivity.playListModels);
                playListAdapter.notifyDataSetChanged();
            }else{
                mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(),"HomeFragment").commit();

            }
           // EventBus.getDefault().removeStickyEvent(playListsModel);

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.search_icon:
                mActivity.toolbar.setVisibility(View.GONE);
                customEditText.setVisibility(View.VISIBLE);
                customEditText.post(new Runnable() {
                    @Override
                    public void run() {
                        customEditText.requestFocus();
                        InputMethodManager imgr = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imgr.showSoftInput(customEditText, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
                break;
            case R.id.img_refresh:
                mActivity.refreshRecord();
                break;


        }

    }

    @Override
    public void onBackPressed() {
        mActivity.hideSoftKeyboard();
        if(customEditText.getVisibility()==View.VISIBLE){
            customEditText.getText().clear();
            customEditText.setVisibility(View.GONE);
            mActivity.toolbar.setVisibility(View.VISIBLE);
            playListAdapter.getFilter().filter("");
            playListAdapter.notifyDataSetChanged() ;
        }else{
            mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(),"HomeFragment").commit();

        }

    }

    @Override
    public void onTimeComplete(String id,int position) {

        if(playListModelList.size()>position) {
            for(int i = 0;i<playListModelList.size();i++){
                if (playListModelList.get(i).getId().equalsIgnoreCase(id)) {
                    playListModelList.get(i).setIsPlaying("false");
                    playListAdapter.notifyDataSetChanged();
                }
            }

        }
    }


    @Override
    public void onItemClick(List<PlayListModel> mDataList, int position) {
        if(mDataList.get(position).getIsPlaying().equalsIgnoreCase("false")) {
            if(!TextUtils.isEmpty(Utility.getPref(mActivity, Constant.LOOP_COUNT,null))){
                Count =Integer.valueOf(Utility.getPref(mActivity, Constant.LOOP_COUNT,null));
            }else{
                Count =1;
            }
            String str = "PlayList" + "&@#¶" + mDataList.get(position).getId() + "&@#¶"  + mDataList.get(position).getName() +"&@#¶"  +Count;
            for(int i = 0; i<playListModelList.size();i++){
                playListModelList.get(i).setIsPlaying("false");
            }
            mDataList.get(position).setIsPlaying("true");
            for(int i =0;i<playListModelList.size();i++){
                if(playListModelList.get(i).getId().equalsIgnoreCase(mDataList.get(position).getId())){
                    playListModelList.get(i).setIsPlaying("true");
                }
            }
            mActivity.mChatService.write(str.getBytes());

         //   mActivity.startTimer(mDataList.get(position).getId(),Integer.toString(Integer.parseInt(mDataList.get(position).getDuration())*Count),position);

            playListAdapter.notifyDataSetChanged();
        }else{
            mDataList.get(position).setIsPlaying("false");
            for(int i =0;i<playListModelList.size();i++){
                if(playListModelList.get(i).getId().equalsIgnoreCase(mDataList.get(position).getId())){
                    playListModelList.get(i).setIsPlaying("false");
                }
            }
            mActivity.cancelTimer();
            mActivity.releasePlayerApp();
            playListAdapter.notifyDataSetChanged();
        }
    }
}
