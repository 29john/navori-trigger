//package com.navori.triggerapp.fragment;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//import android.widget.TextView;
//
//import com.navori.triggerapp.R;
//import com.navori.triggerapp.receiver.ConnectivityReceiver;
//
//import java.util.Locale;
//
//
//
//public class LoginFragment extends Fragment implements View.OnClickListener {
//
//
//    public LoginFragment(){}
//     private View root;
//    private TextView tvSignIn,tvError,tvNext;
//    private boolean isLogin= true;
//    private EditText etUserName,etPassword;
//    private Locale myLocale;
//    private Context mContext;
//
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        root = inflater.inflate(R.layout.login_layout, container, false);
//        mContext =  getActivity();
//
//        tvSignIn =(TextView)root.findViewById(R.id.tv_signin);
//        tvError =(TextView)root.findViewById(R.id.tv_error);
//        tvNext =(TextView)root.findViewById(R.id.tv_next);
//        etUserName =(EditText)root.findViewById(R.id.et_username);
//        etPassword =(EditText)root.findViewById(R.id.et_password);
//
//        tvNext.setOnClickListener(this);
//        tvSignIn.setOnClickListener(this);
//
//
//
//        return root;
//    }
//
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.tv_signin:
//                boolean checkInput = input();
//                if(checkInput) {
//                    if (ConnectivityReceiver.isConnected(mContext)) {
//                        isLogin = true;
//
//                    } else {
//                        mContext.makeToast("Internet not available", mContext);
//                    }
//                }
//                break;
//            case R.id.tv_next:
//                if(isLogin) {
//                    mContext.getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new PlayerNameFragment(),"PlayerNameFragment").commit();
//
//                }else{
//                    mContext.makeToast("Please Login First",mContext);
//                }
//                break;
//
//        }
//    }
//
//    private boolean input() {
//
//        if(TextUtils.isEmpty(etUserName.getText().toString().trim())){
//            mContext.makeToast("Please fill username field",mContext);
//            return false;
//        }
//        if(TextUtils.isEmpty(etPassword.getText().toString().trim())){
//            mContext.makeToast("Please fill password field",mContext);
//            return false;
//        }
//
//
//        return true;
//    }
//}
