package com.navori.triggerapp.fragment;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.navori.triggerapp.R;
import com.navori.triggerapp.activity.HomeActivity;
import com.navori.triggerapp.constant.Constant;
import com.navori.triggerapp.interfaces.OnBackPressed;
import com.navori.triggerapp.utility.Utility;

public class SettingFragment extends Fragment implements View.OnClickListener,OnBackPressed {

    public SettingFragment(){}

    private HomeActivity mContext;
    private View root;
    private TextView tvLangSelection,tvReset,tvNoLoop;
    private ImageView searchIcon;
    private ImageView imgRefresh;
    private TextView tvHeader;
    private  int Count =-1;
    private LinearLayout llLanguages,llLoop;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.setting_layout, container, false);
        mContext = (HomeActivity) getActivity();
        searchIcon = (ImageView)mContext.findViewById(R.id.search_icon);
        searchIcon.setVisibility(View.GONE);
        imgRefresh = (ImageView) mContext. findViewById(R.id.img_refresh);
        imgRefresh.setVisibility(View.GONE);

        mContext.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mContext.showMenuButton();
        tvHeader =(TextView)mContext.findViewById(R.id.header_text);
        tvHeader.setText(R.string.preferences);
        tvHeader.setAllCaps(true);
        tvLangSelection =(TextView)root.findViewById(R.id.tv_language_selection);
        tvReset =(TextView)root.findViewById(R.id.tv_reset);
       tvNoLoop =(TextView)root.findViewById(R.id.tv_set_loop);
        llLanguages =(LinearLayout)root.findViewById(R.id.ll_language);
        llLoop =(LinearLayout)root.findViewById(R.id.ll_loop);
        llLanguages.setOnClickListener(this);
        tvReset .setOnClickListener(this);
        llLoop.setOnClickListener(this);

        if(!TextUtils.isEmpty(Utility.getPref(mContext, Constant.SELECTED_LANGUAGE,null))){
            tvLangSelection.setText(Utility.getPref(mContext, Constant.SELECTED_LANGUAGE,null));
        }
        if(!TextUtils.isEmpty(Utility.getPref(mContext, Constant.LOOP_COUNT,null))){
            tvNoLoop.setText(Utility.getPref(mContext, Constant.LOOP_COUNT,null));
        }


        return root;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_language:
                mContext.getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new SelectionLanguageFragment(), "SelectionLanguageFragment").commit();
                break;

            case R.id.ll_loop:
                openDialog();
                break;
            case R.id.tv_reset:
                mContext.showResetAlert();
                break;
        }
    }

    private void openDialog() {

        final Dialog dialog = new Dialog(mContext,R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_playmedia);
        final EditText editText = (EditText)dialog.findViewById(R.id.et_input);
        TextView tvOk = (TextView)dialog.findViewById(R.id.tv_ok);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( editText.getText().length()>0 ) {
                    Count = Integer.valueOf(editText.getText().toString().trim());
                }else{
                    Count =1;
                }
                Utility.savePref(mContext, Constant.LOOP_COUNT,""+Count);
                tvNoLoop.setText(Utility.getPref(mContext, Constant.LOOP_COUNT,null));
                dialog.dismiss();


            }
        });

        dialog.show();

    }

    @Override
    public void onBackPressed() {
        mContext.getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment(), "HomeFragment").commit();

    }
}
