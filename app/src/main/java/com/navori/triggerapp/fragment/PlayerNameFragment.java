//package com.navori.triggerapp.fragment;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.navori.triggerapp.R;
//import com.navori.triggerapp.activity.HomeActivity;
//import com.navori.triggerapp.constant.Constant;
//import com.navori.triggerapp.utility.Utility;
//
//
//public class PlayerNameFragment extends Fragment implements View.OnClickListener {
//
//    public PlayerNameFragment(){};
//
//     private View root;
//    private TextView tvPlayerName,tvNext;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        root = inflater.inflate(R.layout.player_name, container, false);
//
//        tvPlayerName = (TextView)root. findViewById(R.id.tv_signin);
//        tvNext = (TextView)root. findViewById(R.id.tv_next);
//        tvNext.setOnClickListener(this);
//        return root;
//    }
//
//    @Override
//    public void onClick(View v) {
//        Utility.savePref(mContext, Constant.LOGIN,"Complete");
//        if (mContext.mChatService != null) mContext.mChatService.stop();
//        Intent intent = new Intent(mContext,HomeActivity.class);
//        startActivity(intent);
//        mContext.finish();
//    }
//}
