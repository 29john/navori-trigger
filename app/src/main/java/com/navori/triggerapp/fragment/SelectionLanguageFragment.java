package com.navori.triggerapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.navori.triggerapp.R;
import com.navori.triggerapp.activity.HomeActivity;
import com.navori.triggerapp.constant.Constant;
import com.navori.triggerapp.interfaces.OnBackPressed;
import com.navori.triggerapp.utility.Utility;

import java.util.Arrays;
import java.util.List;


public class SelectionLanguageFragment extends Fragment implements View.OnClickListener ,OnBackPressed{

    public SelectionLanguageFragment(){}

    private HomeActivity mContext;
    private View root;
    private TextView tvOk;
    private String strSelectedLanguage ="";
    private WheelPicker wheelPicker;
    private ImageView searchIcon;
    private ImageView imgRefresh;
    private TextView tvHeader;
    private int pos = 2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.language_selection, container, false);
        mContext = (HomeActivity) getActivity();
        wheelPicker= (WheelPicker)root.findViewById(R.id.wheel_languages);
        tvOk =(TextView)root.findViewById(R.id.tv_ok);
        tvOk.setOnClickListener(this);
        searchIcon = (ImageView)mContext.findViewById(R.id.search_icon);
        searchIcon.setVisibility(View.GONE);
        imgRefresh = (ImageView) mContext. findViewById(R.id.img_refresh);
        imgRefresh.setVisibility(View.GONE);

        mContext.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mContext.showMenuButton();
        tvHeader =(TextView)mContext.findViewById(R.id.header_text);
        tvHeader.setText(R.string.preferences);
        String[] language_array = getResources().getStringArray(R.array.languages);
        List<String> data = Arrays.asList(language_array);

        wheelPicker.setData(data);
        if(TextUtils.isEmpty(Utility.getPref(mContext,Constant.POSITION,null))){
            wheelPicker.setSelectedItemPosition(2);
            strSelectedLanguage ="English";
        }else{
            wheelPicker.setSelectedItemPosition(Integer.valueOf(Utility.getPref(mContext,Constant.POSITION,null)));
        }

        wheelPicker.setCyclic(false);
        wheelPicker.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                strSelectedLanguage = String.valueOf(data);
                pos = position;
            }
        });

        return root;
    }

    @Override
    public void onClick(View v) {
        if(!strSelectedLanguage.equalsIgnoreCase("")){
            Utility.savePref(mContext, Constant.SELECTED_LANGUAGE,strSelectedLanguage);
            Utility.savePref(mContext,Constant.POSITION,""+pos);
            Utility.savePref(mContext,Constant.SELECTED_LANGUAGE_CODE,Utility.getLanguageCode(strSelectedLanguage));
            Utility.setupLanguage(mContext,Utility.getLanguageCode(strSelectedLanguage));
            if(mContext instanceof  HomeActivity){
                Intent intent = mContext.getIntent();
                mContext.finish();
                startActivity(intent);
            }

        }else{
            mContext.makeToast("Please select any language",mContext);
        }
    }

    @Override
    public void onBackPressed() {
        mContext.getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new SettingFragment(), "SettingFragment").commit();

    }
}
