package com.navori.triggerapp.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.navori.triggerapp.R;
import com.navori.triggerapp.model.DeviceListModel;
import com.navori.triggerapp.model.ItemModel;
import com.navori.triggerapp.utility.Utility;

import java.util.List;

public class DeviceListAdapter extends BaseAdapter {


    private Context context;
     private List<DeviceListModel> mDataList = null;


    public DeviceListAdapter(Context context, List<DeviceListModel> items) {
        this.context = context;
        this.mDataList = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        private TextView tvName,tvMacAddress;
    }


    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.device_item, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tvMacAddress = (TextView) convertView.findViewById(R.id.tv_mac_address);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        DeviceListModel deviceListModel = mDataList.get(position);
        holder.tvName.setText(deviceListModel.getDeviceName());
        holder.tvMacAddress.setText("Mac: "+deviceListModel.getMacAddress());
        return convertView;
    }
}
