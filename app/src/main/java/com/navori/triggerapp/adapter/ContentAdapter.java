package com.navori.triggerapp.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.navori.triggerapp.R;
import com.navori.triggerapp.model.ItemModel;
import com.navori.triggerapp.utility.Utility;

import java.util.ArrayList;
import java.util.List;

public class ContentAdapter extends BaseAdapter implements Filterable {


    private Context context;
    private ValueFilter valueFilter;
    private List<ItemModel> originalData = null;
    private List<ItemModel>filteredData = null;
    private MyClickListener myClickListener;
    public ContentAdapter(Context context, List<ItemModel> items,MyClickListener myClickListener) {
        this.context = context;
        this.originalData = items;
        this.filteredData = items;
        this.myClickListener = myClickListener;
    }

    /*private view holder class*/
    private class ViewHolder {
        private TextView tvName;
        private TextView tvType;
        private ImageView imageView;
        private RelativeLayout llItem;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_item, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvType = (TextView) convertView.findViewById(R.id.tv_desc);
            holder.imageView = (ImageView) convertView.findViewById(R.id.img_stop);
            holder.llItem =(RelativeLayout) convertView.findViewById(R.id.ll_item);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        ItemModel rowItem = filteredData.get(position);
        holder.tvName.setText(rowItem.getName());
        holder.tvType.setText(Utility.convertTime(rowItem.getDuration()));
        if(rowItem.getIsPlaying().equalsIgnoreCase("true")){
            holder.imageView.setVisibility(View.VISIBLE);
            holder.llItem.setBackgroundResource(R.drawable.rounded_bg);

        }else{
            holder.imageView.setVisibility(View.GONE);
            holder.llItem.setBackgroundResource(R.color.white);
        }

        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             myClickListener.onItemClick(filteredData,position);
            }
        });
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<ItemModel> list = originalData;

            int count = list.size();
            final ArrayList<ItemModel> nlist = new ArrayList<ItemModel>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();


            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            filteredData = (ArrayList<ItemModel>) results.values;
            notifyDataSetChanged();
        }

    }
    public interface MyClickListener {
        void onItemClick(List<ItemModel> mDataList, int position);
    }
}
