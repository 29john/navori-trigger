package com.navori.triggerapp.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.navori.triggerapp.R;
import com.navori.triggerapp.activity.HomeActivity;
import com.navori.triggerapp.model.ItemModel;
import com.navori.triggerapp.utility.Utility;

import java.util.List;

public class GridAdapter extends BaseAdapter{



    private HomeActivity context;
    private List<String> countData = null;


    public GridAdapter(Context context, List<String> items) {
        this.context = (HomeActivity) context;
        this.countData = items;

    }

    /*private view holder class*/
    private class ViewHolder {
        private TextView tvName;
        private TextView tvCount;
        private ImageView imageView;
    }




    @Override
    public int getCount() {
        return countData.size();
    }

    @Override
    public Object getItem(int position) {
        return countData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.grid_item, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvCount = (TextView) convertView.findViewById(R.id.tv_count);
            holder.imageView = (ImageView) convertView.findViewById(R.id.img_item);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        switch (position){

            case 0:
                holder.tvName.setText(R.string.content);
                if(context.nonTriggerModels.size()>0){
                    holder.tvCount.setText(""+context.nonTriggerModels.size());
                }else {
                    holder.tvCount.setVisibility(View.GONE);
                }

                holder.imageView.setImageResource(R.drawable.contents);
                break;

            case 1:
                holder.tvName.setText(R.string.playlist);
                if(context.playListModels.size()>0){
                    holder.tvCount.setText(""+context.playListModels.size());
                }else {
                    holder.tvCount.setVisibility(View.GONE);
                }
                holder.imageView.setImageResource(R.drawable.playlist);
                break;
            case 2:
                holder.tvName.setText(R.string.trigger);
                if(context.triggerModels.size()>0){
                    holder.tvCount.setText(""+context.triggerModels.size());
                }else {
                    holder.tvCount.setVisibility(View.GONE);
                }
                holder.imageView.setImageResource(R.drawable.trigger);
                break;

        }

        return convertView;
    }
}
